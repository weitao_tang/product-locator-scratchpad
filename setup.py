# Standard Library
from os import environ

# Third-Party Libraries
from setuptools import find_packages, setup

extras = {
    "dev": ["pre-commit == 2.18.1"],
    "test": ["coverage == 6.3.2", "ddt == 1.4.4"],
}

# Allow to install all the dependencies at once
extras["all"] = [item for group in extras.values() for item in group]

setup(
    name="product-locator",
    use_scm_version={
        # Containerized installs typically don't contain scm, so fallback to
        # an env variable that the container can provide at build time.
        "fallback_version": environ.get("AIFI_PY_EXAMPLE_VERSION", "0.0.0")
    },
    license="AiFi (C) All rights reserved",
    long_description=__doc__,
    packages=find_packages(exclude=["tests", "tests.*"]),
    install_requires=[
        # Having trouble with installing something? Checkout CONTRIBUTING.md
        # to learn how to point pip at AiFi's private package registry.
        "aifi-proto-tools",
        "aifi-geometry",
        "aifi-visualization",
        "aifi-log ~= 0.5",
        "typer ~= 0.3",
    ],
    extras_require=extras,
    entry_points={
        "console_scripts": [
            "example-project = example_project.main:main",
        ]
    },
)
