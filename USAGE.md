# `example-project`

**Usage**:

```console
$ example-project [OPTIONS] COMMAND [ARGS]...
```

**Options**:

* `--help`: Show this message and exit.

**Commands**:

* `hello`: Simple hello world example
* `progress`: Example showing typer's progress bar
* `version`: Print the currently installed version of the...

## `example-project hello`

Simple hello world example

**Usage**:

```console
$ example-project hello [OPTIONS]
```

**Options**:

* `--name TEXT`: [env var: NAME; default: world]
* `--help`: Show this message and exit.

## `example-project progress`

Example showing typer's progress bar

**Usage**:

```console
$ example-project progress [OPTIONS]
```

**Options**:

* `--help`: Show this message and exit.

## `example-project version`

Print the currently installed version of the application

**Usage**:

```console
$ example-project version [OPTIONS]
```

**Options**:

* `--help`: Show this message and exit.
