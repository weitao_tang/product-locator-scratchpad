# IMPORTANT - READ FIRST
_This is the scratchpad for product locator, the code is messy and does not follow the template.

It includes the tasks of 
1. TAPD-1438 Implement automatic shelf line cropping for DTW classification
2. TAPD-1856 FCA for shelf index estimation
3. TAPD-1850 Identify the shelf index for the cluster of products using vision matching
4. TAPD-1858 Estimate the planogram combing vision and location pior
5. TAPD-2062 Optimize 3d projection - verify and adjust the store layout
6. TAPD-2