import numpy as np
a = np.arange(9)
b = a.reshape((3,3))
print(b)

b = np.exp(b)
print(b)

c = b/b.sum(axis=0, keepdims=True)
print(c)