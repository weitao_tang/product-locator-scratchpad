import numpy as np
import os
import torch
import torch.nn as nn
import torch.utils.data
import torch.utils.data.distributed
import torch.nn.functional as F
from mmcv.utils.config import Config
from collections import OrderedDict
import shutil
import urllib
import cv2
from tqdm import tqdm

from product_recognition.net_utils import load_model_
from product_recognition.models import build_backbone, build_classifier
from product_recognition.dataset import build_transform

from camera_view_process.data import Planogram

class VisionClassifier():
    def __init__(self, model_path, num_classes, class_dict, planogram = None, image_bank_dir = None):
        backbone = build_backbone(arch = dict(name="resnet50", kwargs={}), num_features= 512)
        classifier = build_classifier(num_classes=num_classes, arch= dict(name="linear", kwargs={}), num_features=512)
        self.model = nn.Sequential(OrderedDict([("backbone", backbone), ("classifier", classifier)])).cuda()
        _ = load_model_(model_path, self.model)
        self.barcode2class = class_dict
        self.num_classes = num_classes
        self.valid_transformation = build_transform(112, True)
        self.softmax = torch.nn.Softmax(dim=0)
        self.class2barcode = self.obtain_class2barcode()
        if planogram is not None:
            assert isinstance(planogram, Planogram)
            self.planogram = planogram
            self.planogram_features, self.planogram_product_id2barcode, self.planogram_empty_ids = self.obtain_planogram_features()
        if image_bank_dir is not None:
            print(image_bank_dir)
            assert os.path.isdir(image_bank_dir)
            self.image_bank_dir = image_bank_dir
            self.image_bank_features, self.image_bank_barcodes = self.obtain_image_bank_features()

    def obtain_class2barcode(self):
        class2barcode = {}
        for barcode, class_ in self.barcode2class.items():
            class2barcode[class_] = barcode
        return class2barcode

    def obtain_feature(self, image):
        self.model.eval()
        image = self.valid_transformation(image).cuda()
        image = torch.unsqueeze(image, dim=0)
        with torch.no_grad():
            logits = self.model(image)[0].cpu()
        return logits

    def inference(self, image):
        logits = self.obtain_feature(image)
        pred = torch.argmax(logits, dim=0).item()
        prob = self.softmax(logits).cpu().numpy()
        barcode = self.class2barcode[pred]
        return pred, prob, barcode

    def obtain_planogram_features(self):
        total_products = len(self.planogram.planogram_dict.keys())
        planogram_features = np.zeros((total_products, 512))
        self.model.eval()
        planogram_product_id2barcode = {}
        planogram_empty_ids = []
        with torch.no_grad():
            for product_id, (_, info) in tqdm(enumerate(self.planogram.planogram_dict.items())):
                barcode = info['productId']['id']
                image_path = self.planogram.barcode2image_path[barcode]
                if image_path is not None:
                    assert os.path.isfile(image_path)
                    image = cv2.imread(image_path)
                    image = self.valid_transformation(image).cuda()
                    image = torch.unsqueeze(image, dim=0)
                    feature = self.model.backbone(image)
                    feature_norm = F.normalize(feature).cpu().numpy()
                else:
                    print(barcode, "has not image")
                    feature_norm = np.zeros((1, 512))
                    planogram_empty_ids.append(product_id)
                planogram_features[product_id, :] = feature_norm
                planogram_product_id2barcode[product_id] = barcode
        return planogram_features, planogram_product_id2barcode, planogram_empty_ids

    def obtain_image_bank_features(self):
        image_bank_size = 0
        for _, _, files in os.walk(self.image_bank_dir):
            image_bank_size += len(files)
        print('image_bank_size:', image_bank_size)
        image_bank_features = np.zeros((image_bank_size, 512))
        image_id = 0
        image_bank_barcodes = []
        with torch.no_grad():
            for barcode in os.listdir(self.image_bank_dir):
                barcode_dir = os.path.join(self.image_bank_dir, barcode)
                for image in os.listdir(barcode_dir):
                    image_path = os.path.join(barcode_dir, image)
                    image = cv2.imread(image_path)
                    image = self.valid_transformation(image).cuda()
                    image = torch.unsqueeze(image, dim=0)
                    feature = self.model.backbone(image)
                    feature_norm = F.normalize(feature).cpu().numpy()
                    image_bank_features[image_id,:] = feature_norm
                    image_id += 1
                    image_bank_barcodes.append(barcode)
        assert image_id == image_bank_size # check all images loaded
        return image_bank_features, image_bank_barcodes

    ################ obtain ids ###############
    def obtain_id_for_planogram_feature_by_barcode(self, barcode):
        id_ = -1
        for product_id, planogram_barcode in self.planogram_product_id2barcode.items():
            if planogram_barcode == barcode:
                id_ = product_id
        if id_ != -1:
            return id_
        else:
            print("barcode ", barcode, "does not exist!")
            raise ValueError # barcode

    def obtain_id_for_feature_bank_features_by_barcode(self, barcode):
        column_ids = []
        for column_id, image_bank_barcode in enumerate(self.image_bank_barcodes):
            if image_bank_barcode == barcode:
                column_ids.append(column_id)
        return column_ids

    ################ similarity calculation #################
    def feature_similarity_with_planogram(self, images):
        assert self.planogram is not None
        self.model.eval()
        with torch.no_grad():
            all_images = []
            for image in images:
                image = self.valid_transformation(image).cuda()
                all_images.append(image)
            if len(all_images) == 1:
                tensor_images = torch.unsqueeze(all_images[0], dim=0)
            else:
                tensor_images = torch.stack(all_images)
            features = self.model.backbone(tensor_images)
            feature_norms = F.normalize(features).cpu().numpy()
            sim = np.matmul(feature_norms, self.planogram_features.T)
            sim[:, self.planogram_empty_ids] = -1 
        return sim

    def feature_similarity_with_barcode_by_planogram(self, images, barcode):
        assert self.planogram is not None
        self.model.eval()
        id_ = self.obtain_id_for_planogram_feature_by_barcode(barcode)
        if id_ not in self.planogram_empty_ids:    
            with torch.no_grad():
                all_images = []
                for image in images:
                    image = self.valid_transformation(image).cuda()
                    all_images.append(image)
                if len(all_images) == 1:
                    tensor_images = torch.unsqueeze(all_images[0], dim=0)
                else:
                    tensor_images = torch.stack(all_images)
                features = self.model.backbone(tensor_images)
                feature_norms = F.normalize(features).cpu().numpy()
                sim = np.matmul(feature_norms, self.planogram_features[id_, :].T)
        else:
            sim = np.ones((len(images))) * -1
        return sim

    def feature_simialrity_with_image_bank(self, images):
        assert self.image_bank_dir is not None
        self.model.eval()
        with torch.no_grad():
            with torch.no_grad():
                all_images = []
                for image in images:
                    image = self.valid_transformation(image).cuda()
                    all_images.append(image)
                if len(all_images) == 1:
                    tensor_images = torch.unsqueeze(all_images[0], dim=0)
                else:
                    # tensor_images = torch.cat(all_images)
                    tensor_images = torch.stack(all_images)
                print(tensor_images.shape)
                features = self.model.backbone(tensor_images)
                feature_norms = F.normalize(features).cpu().numpy()
                sim = np.matmul(feature_norms, self.image_bank_features.T)
        return sim

    def feature_similarity_with_barcode_by_image_bank(self, images, barcode):
        ids = self.obtain_id_for_feature_bank_features_by_barcode(barcode)
        self.model.eval()
        with torch.no_grad():
            all_images = []
            for image in images:
                image = self.valid_transformation(image).cuda()
                all_images.append(image)
            if len(all_images) == 1:
                tensor_images = torch.unsqueeze(all_images[0], dim=0)
            else:
                tensor_images = torch.stack(all_images)
            features = self.model.backbone(tensor_images)
            feature_norms = F.normalize(features).cpu().numpy()
            sim = np.matmul(feature_norms, self.image_bank_features[ids, :].T)
            sim = np.max(sim, axis=1)
        return sim

    def obtain_classes_of_barcodes(self, barcodes):
        classes = []
        for barcode in barcodes:
            if barcode in self.barcode2class:
                classes.append(self.barcode2class[barcode])
            else:
                classes.append(-1) # barcodes does not exist in classifier
        return classes