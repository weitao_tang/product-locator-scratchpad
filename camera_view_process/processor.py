from cProfile import label
from dataclasses import dataclass
from ntpath import join
from turtle import width
from joblib import parallel_backend
import matplotlib.pyplot as plt
from proto_tools.codec import StoreCalibration
from proto_tools.codec.components import GondolaId,ShelfId,BinId
from geometry import Camera
from geometry.utils_2d.polygon import Polygon2d
from geometry.box import BoxFace
import numpy as np
import copy
from aifi_visualization.constants import BLUE, GREEN, GREY, ORANGE, RED
from aifi_visualization.utils import enlarge_fov
from typing import Collection, Dict, Iterable, List, Sequence, Tuple, Union
from aifi_visualization.visualize_components import (
    LineStyle,
    TextStyle,
    visualize_box,
    visualize_component
)
from psutil import disk_partitions
from camera_view_process.visionclassifier import VisionClassifier
from camera_view_process.dtw_predictor import DTWPredictor
import math
import os

import cv2
from pygments import highlight
from sklearn import cluster
from camera_view_process.data import ProductQuery, CameraViewQuery, Planogram
from camera_view_process.visionclassifier import VisionClassifier
from camera_view_process.utils import obtain_colors_from_color_map, obtain_vector_3d, obtain_points_on_lineseg_3d, obtain_distance2line3d, rescale_image_fix_ratio, obtain_projection2line2d, join_images

class CameraViewProcessor():
    """ Using the bounding box of all products from a camera view to locate the gondola shelf of each product 
    """
    def __init__(self, store_calibration_path: str, 
                        camera_maps: Dict[int, List[int]], 
                        vision_classifier: VisionClassifier, 
                        planogram: Planogram,
                        dtw_predictor: DTWPredictor,
                        camera_view_hw = [1080,1920],
                        shelf_match_methods_dict =  {"classifier":{"ratio":0.5}},
                        use_bottom_point = False,
                        debug = False,
                        ):
        with open(store_calibration_path) as fp:
            self.store = StoreCalibration.from_json(fp.read())
        self.store.cameras = self.store.cameras.to_meters()
        self.cameras = self.store.cameras
        self.camera_maps = camera_maps
        self.camera_id2gondola_id = self.obtain_camera_id2gondola_id(camera_maps)
        self.camera_view_hw = camera_view_hw
        self.cameraId2gondolaId2polygon = self.obtain_cameraId2gondolaId2polygon()
        self.cluster_lines = {}
        self.vision_classifier = vision_classifier
        self.planogram = planogram
        self.debug = debug
        self.shelf_match_methods_dict = shelf_match_methods_dict
        self.use_bottom_point = use_bottom_point
        self.dtw_predictor = dtw_predictor

    #########################################################
    ########## major functions to process gondola ###########
    #########################################################

    def obtain_cameraId2gondolaId2polygon(self):
        cameraId2gondolaId2polygon = {}
        for camera_id in self.store.camera_ids:
            camera = self.cameras[camera_id]
            height, width = self.camera_view_hw
            base_polygon  = Polygon2d([np.array([0,0]),
                            np.array([width, 0]),
                            np.array([width, height]),
                            np.array([0, height])])
            gondolaId2polygon = {}
            for gondolaId in self.store.gondolas.keys():
                gondola_box = self.store.bounding_boxes[gondolaId]
                
                # check if face contour is valid
                visible = np.any(
                    camera.are_points_visible(gondola_box.corners, margin=0.0)
                )
                facing_camera = (
                    np.dot(camera.forward_vector, gondola_box.forward_vector) < 0.0
                )
                valid = visible and facing_camera

                if valid:
                    face_contour_3d = gondola_box.get_face_contour(n=5) 
                    face_contour_2d = camera.project_polygon(face_contour_3d)
                    face_contour_2d_scaled = face_contour_2d * np.array([width, height])
                    gondola_polygon = Polygon2d(face_contour_2d_scaled)
                    gondola_polygon_ = gondola_polygon.intersect(base_polygon)
                    assert len(gondola_polygon_)
                    gondola_polygon = gondola_polygon_[0]
                else:
                    gondola_polygon = None

                gondolaId2polygon[gondolaId] = gondola_polygon
            cameraId2gondolaId2polygon[camera_id] = gondolaId2polygon
        return cameraId2gondolaId2polygon

    def locate2gondola_byoverlap(self, camear_view_query, visualize_dir = None):
        '''locate all products in the camear_view_query to gondola by max overlap
        '''
        
        image = camear_view_query.image.copy()
        height, width, _ = image.shape
        camera_id = camear_view_query.camera_id
        productquery_id2gondolaID = {}
        gondolaID_list = list(self.store.gondolas.keys())
        for productquery in camear_view_query.productqueries:
            intersection_areas = []
            prodcutpolygon = Polygon2d(productquery.contour)
            for gondolaId in self.store.gondolas.keys():
                gondola_polygon = self.cameraId2gondolaId2polygon[camera_id][gondolaId]
                if gondola_polygon is not None:
                    intersection_areas.append(prodcutpolygon.get_intersection_area(gondola_polygon))
                else:
                    intersection_areas.append(0)
            productgondolaID = gondolaID_list[np.argmax(np.array(intersection_areas))]
            maxintersec_area = max(intersection_areas)
            if maxintersec_area != 0:
                productquery_id2gondolaID[productquery.product_id] = productgondolaID
                camear_view_query.productqueries[productquery.product_id].pred_gondolaID = productgondolaID
            else:
                productquery_id2gondolaID[productquery.product_id] = None
        if visualize_dir is not None:
            # obtain the color dict: gondolaID2color
            color_bar = np.array(list(range(255)), dtype="uint8").reshape((-1, 1, 1))
            color_bar = cv2.applyColorMap(color_bar, cv2.COLORMAP_HSV)
            total_gondolas = len(self.store.gondolas.keys())
            gondolaID2color = {}
            gap = 255 // total_gondolas
            for i, gondolaId in enumerate(self.store.gondolas.keys()):
                gondolaID2color[gondolaId] = color_bar[gap * i, :, :].tolist()[0]
            
            # plot box by color
            for productquery in camear_view_query.productqueries:
                prodcutgondolaID = productquery_id2gondolaID[productquery.product_id]
                if prodcutgondolaID is not None:
                    color = gondolaID2color[prodcutgondolaID]
                else:
                    color = [120,120,120]
                x,y,w,h = productquery.bbox
                xw = x+w if x+w<width else width
                yh = y+h if y+h<height else height
                image = cv2.rectangle(image, (x,y), (xw,yh), color, thickness=2)
            
            # plot gondola contour
            all_boxes = self.store.bounding_boxes 
            all_gondola_boxes = {boxId: box for boxId, box in all_boxes.items() if boxId in self.store.gondolas.keys()}
            camera = self.cameras[camear_view_query.camera_id]
            scale = 2.0
            image, camera = enlarge_fov(image, camera, scale=scale)
            for component_id, box in all_gondola_boxes.items():
                gondola_id = component_id.gondola_id
                color = gondolaID2color[GondolaId(gondola_id)]
                visualize_component(
                    image,
                    camera,
                    component_box=box,
                    line_style=LineStyle(GREY, 1),
                    highlight=LineStyle(color, 3),
                    text=str(component_id.pretty()),
                    text_style=TextStyle(RED),
                    margin=0.5 / scale,
                )
            os.makedirs(visualize_dir, exist_ok=True)
            dest_path = os.path.join(visualize_dir, f"camera_{camear_view_query.camera_id}_gondola_locate_plots.jpg")
            cv2.imwrite(dest_path, image)
        return productquery_id2gondolaID


    ########################################################
    ########## major functions to process shelf ############
    ########################################################

    def cluster_on_shelf(self, camera_view_query, gondolaID, merge_cluster_ratio = 0.4):
        """ perform clustering and merge on shelf products
        """
        product_ids = []
        dists = []
        for productquery in camera_view_query.productqueries:
            if productquery.pred_gondolaID == gondolaID:
                dist_to_bottom = productquery.dist_to_bottom
                assert dist_to_bottom is not None
                dists.append([dist_to_bottom])
                product_ids.append(productquery.product_id)

        dists = np.array(dists)

        # obtain how many shelves
        k = len(self.store.gondolas[gondolaID.gondola_id].shelf_ids)

        # clustering to split product into different shelves
        from sklearn.cluster import KMeans
        kmeans = KMeans(n_clusters=k, random_state=0).fit(dists)

        # merge clusters that are too close, this can be caused by occluded shelf
        cluster_centers = [center[0] for center in kmeans.cluster_centers_]
        cluster_centers_sorted = np.sort(cluster_centers)
        
        cluster_gaps = np.array([cluster_centers_sorted[i+1] - cluster_centers_sorted[i] for i in range(len(cluster_centers_sorted) - 1)])
        median_gap = np.median(cluster_gaps)
        merge_threshold = median_gap * merge_cluster_ratio

        self.debug_print("cluster_centers:")
        self.debug_print(cluster_centers_sorted)

        print("cluster_gaps", cluster_gaps)
        print("median gap ", median_gap, "merge threshold", merge_threshold)

        if_merge = list(cluster_gaps < merge_threshold)
        print("if merge", if_merge)
        print(len(cluster_centers_sorted))
        cluster_id_sorted = list(np.argsort(cluster_centers))
        cluster_id2merged_id = {cluster_id_sorted[0]:0} 
        merged_id = 0
        for cluster_id, merge in zip(cluster_id_sorted[1:], if_merge):
            if not merge:
                merged_id += 1
            cluster_id2merged_id[cluster_id] = merged_id

        print("cluster_id2merged_id", cluster_id2merged_id)
        
        # generate merged cluster centers
        preds = kmeans.predict(dists)
        merged_preds = -1 * np.ones(preds.shape, dtype = np.int)
        merged_id2sumcount = {}
        for cluster_id, merged_id in cluster_id2merged_id.items():
            count = sum(preds == cluster_id)
            dist_sum = sum(dists[preds == cluster_id])
            merged_preds[preds == cluster_id] = merged_id
            if merged_id in merged_id2sumcount.keys():
                merged_id2sumcount[merged_id][0] += dist_sum
                merged_id2sumcount[merged_id][1] += count
            else:
                merged_id2sumcount[merged_id] = [dist_sum, count]
        
        merged_id2merged_center = {merged_id:info[0]/info[1] for merged_id, info in merged_id2sumcount.items()}
        print("merged_id2merged_center", merged_id2merged_center)

        product_id2merged_id = {}
        for product_id, pred, merged_pred in zip(product_ids, preds, merged_preds):
            product_id2merged_id[product_id] = merged_pred
            camera_view_query.productqueries[product_id].shelf_cluster_id = pred
            camera_view_query.productqueries[product_id].merged_shelf_cluster_id = pred

        # product_id2merged_id = {product_id:merged_pred for product_id, merged_pred in zip(product_ids,  merged_preds)}
        print("product_id2merged_id", product_id2merged_id)

        merged_id2product_id = {}
        for product_id, merged_cluster in product_id2merged_id.items():
            if merged_cluster in merged_id2product_id.keys():
                merged_id2product_id[merged_cluster].append(product_id)
            else:
                merged_id2product_id[merged_cluster] = [product_id]
        
        print("merged_id2product_id", merged_id2product_id)

        return merged_id2merged_center, product_id2merged_id, merged_id2product_id

    def match_merged_to_shelf(self, camera_view_query, 
                                    merged_id2merged_center, 
                                    gondolaID, 
                                    product_id2merged_id, 
                                    merged_id2product_id,
                                    shelf_ids):
        total_merges = len(merged_id2merged_center.keys())
        total_shelves = len(shelf_ids)
        merged2shelf_sim = np.zeros((total_merges, total_shelves))
        if "classifier" in self.shelf_match_methods_dict.keys():
            merged2shelf_sim_classifier = self.obtain_prodcuts2shelf_similarity_by_classifier(camera_view_query, merged_id2merged_center, 
                        shelf_ids, product_id2merged_id, merged_id2product_id, gondolaID)
            print("classifier_sim", merged2shelf_sim_classifier)
            merged2shelf_sim_classifier *= self.shelf_match_methods_dict["classifier"]["ratio"]
        else:
            merged2shelf_sim_classifier = np.zeros((total_merges, total_shelves))

        if "feature_sim" in self.shelf_match_methods_dict.keys():
            merged2shelf_sim_planogram_feature = self.obtain_prodcuts2shelf_similarity_by_planogram_feature(camera_view_query, merged_id2merged_center, 
                        shelf_ids, merged_id2product_id, gondolaID)
            print("feature_sim", merged2shelf_sim_planogram_feature)
            merged2shelf_sim_planogram_feature *= self.shelf_match_methods_dict["feature_sim"]["ratio"]
        else:
            merged2shelf_sim_planogram_feature = np.zeros((total_merges, total_shelves))

        ### image bank

        if "bank_sim" in self.shelf_match_methods_dict.keys():
            merged2shelf_sim_bank_feature = self.obtain_prodcuts2shelf_similarity_by_image_bank_feature(camera_view_query, merged_id2merged_center, 
                        shelf_ids, merged_id2product_id, gondolaID)
            merged2shelf_sim_bank_feature *= self.shelf_match_methods_dict["bank_sim"]["ratio"]
            print("bank_sim", merged2shelf_sim_bank_feature)
        else:
            merged2shelf_sim_bank_feature = np.zeros((total_merges, total_shelves))

        merged2shelf_sim = merged2shelf_sim_classifier + merged2shelf_sim_planogram_feature + merged2shelf_sim_bank_feature
        print('merged2shelf_sim ', merged2shelf_sim)

        # find merged cluster that match to shelf
        max_shelf_sim = np.max(merged2shelf_sim, axis = 1) # the maximum similarity for each merged cluster
        print("max_shelf_sim", max_shelf_sim)
        if_match = max_shelf_sim >  self.shelf_match_methods_dict["threshold"]
        print("if_match", if_match, "sum", sum(if_match))
        if sum(if_match) == 0:
            print("no match found!!!!!!!")
        max_sim_shelf = np.argmax(merged2shelf_sim, axis = 1) + 1 # the shelf with max similarity, notice shelf_id starts with 1
        print("max_sim_shelf", max_sim_shelf)

        # obtain merged_id and shelf_id pair
        merge_id_shelf_id_list = []
        unmatched_merge_ids = []
        for merge_id, match in enumerate(if_match):
            if match:
                merge_id_shelf_id_list.append((merge_id, max_sim_shelf[merge_id]))
            else:
                unmatched_merge_ids.append(merge_id)
        
        matched_shelves = [pair[1] for pair in merge_id_shelf_id_list]
        ascending = sorted(matched_shelves) == matched_shelves
        assert ascending
        print(merge_id_shelf_id_list)

        def sort_func(merge_id_shelf_id_pair):
            return merge_id_shelf_id_pair[0]
        
        merge_id_shelf_id_list.sort(key = sort_func)
        tmp_list = [shelf_id for _, shelf_id in merge_id_shelf_id_list]
        assert tmp_list == sorted(tmp_list) # shelf_id and merge_id should be both ascending

        # obtain matched shelf_line heights (distance to bottome of the gondola)
        matched_shelf_id2shelf_line = {}
        for merge_id, shelf_id in merge_id_shelf_id_list:
            shelf_line = merged_id2merged_center[merge_id]
            matched_shelf_id2shelf_line[shelf_id] = shelf_line
        print("matched_shelf_id2shelf_line", matched_shelf_id2shelf_line)
        
        # obtain unmatched heights
        unmatched_merge_ids2center = {}
        for merge_id in unmatched_merge_ids:
            unmatched_merge_ids2center[merge_id] = merged_id2merged_center[merge_id]
        print("unmatched_merge_ids2center", unmatched_merge_ids2center)

        return matched_shelf_id2shelf_line, unmatched_merge_ids2center

    def obtain_prodcuts2shelf_similarity_by_planogram_feature(self, camera_view_query, merged_id2merged_center, 
                        shelf_ids, merged_id2product_id, gondolaID):
        total_merges = len(merged_id2merged_center.keys())
        total_shelves = len(shelf_ids)

        shelf_id2barcodes = {}
        for shelf_id in shelf_ids:
            shelf_barcodes = self.planogram.obtain_barcodes_by_loc(gondolaID.gondola_id,shelf_id)
            shelf_id2barcodes[shelf_id] = shelf_barcodes

        print("shelf_id2barcodes", shelf_id2barcodes)

        # obtain the columns of product2planogram similarity matrix which corresponds to each shelf
        shelf_id2sim_columns = {}
        for shelf_id, shelf_barcodes in shelf_id2barcodes.items():
            column_ids = []
            for product_id, barcode in self.vision_classifier.planogram_product_id2barcode.items():
                if barcode in shelf_barcodes:
                    column_ids.append(product_id)
            shelf_id2sim_columns[shelf_id] = np.array(column_ids)

        print("shelf_id2sim_columns", shelf_id2sim_columns)

        # obtain the product2planogram similarity matrix
        merged2shelf_sim_feature = np.zeros((total_merges, total_shelves))
        for merged_id, product_ids in merged_id2product_id.items():
            product_images = [camera_view_query.productqueries[product_id].image for product_id in product_ids]
            product2planogram_sim = self.vision_classifier.feature_similarity_with_planogram(product_images)
            for shelf_id, sim_columns in shelf_id2sim_columns.items():
                print("sim_columns", sim_columns)
                product2shelf_sim_all_barcodes = product2planogram_sim[:, sim_columns]
                product2shelf_sim_max = np.max(product2shelf_sim_all_barcodes, axis=1)
                product2shelf_sim_avg = np.average(product2shelf_sim_max)
                merged2shelf_sim_feature[merged_id, shelf_id-1] = product2shelf_sim_avg
        return merged2shelf_sim_feature

    def obtain_prodcuts2shelf_similarity_by_image_bank_feature(self, camera_view_query, merged_id2merged_center, 
                    shelf_ids, merged_id2product_id, gondolaID):
        total_merges = len(merged_id2merged_center.keys())
        total_shelves = len(shelf_ids)

        shelf_id2barcodes = {}
        for shelf_id in shelf_ids:
            shelf_barcodes = self.planogram.obtain_barcodes_by_loc(gondolaID.gondola_id,shelf_id)
            shelf_id2barcodes[shelf_id] = shelf_barcodes

        # obtain the columns of product2planogram similarity matrix which corresponds to each shelf
        shelf_id2sim_columns = {}
        for shelf_id, shelf_barcodes in shelf_id2barcodes.items():
            column_ids = []
            for column_id, barcode in enumerate(self.vision_classifier.image_bank_barcodes):
                if barcode in shelf_barcodes:
                    column_ids.append(column_id)
            shelf_id2sim_columns[shelf_id] = np.array(column_ids)
        

        merged2shelf_sim_feature = np.zeros((total_merges, total_shelves))

        # obtain the product2planogram similarity matrix by normalized sum of similarity
        for merged_id, product_ids in merged_id2product_id.items():
            product_images = [camera_view_query.productqueries[product_id].image for product_id in product_ids]
            product2bank_sim = self.vision_classifier.feature_simialrity_with_image_bank(product_images)
            for shelf_id, sim_columns in shelf_id2sim_columns.items():
                sim_columns = list(sim_columns)
                if len(sim_columns) != 0:
                    product2shelf_sim_all = product2bank_sim[:, sim_columns]
                    product2shelf_sim_max = np.max(product2shelf_sim_all, axis=1)
                    product2shelf_sim_avg = np.average(product2shelf_sim_max)
                else:
                    product2shelf_sim_avg = -1 # cosine similarity, lower bound -1
                merged2shelf_sim_feature[merged_id, shelf_id-1] = product2shelf_sim_avg
        return merged2shelf_sim_feature

    def obtain_prodcuts2shelf_similarity_by_classifier(self, camera_view_query, merged_id2merged_center, 
                        shelf_ids, product_id2merged_id, merged_id2product_id, gondolaID):
        total_merges = len(merged_id2merged_center.keys())
        total_shelves = len(shelf_ids)
        merged2shelf_sim_classifier = np.zeros((total_merges, total_shelves))
        product_id2vision_pred = {}
        for product_id in product_id2merged_id.keys():
            _, _, barcode = self.vision_classifier.inference(camera_view_query.productqueries[product_id].image)
            product_id2vision_pred[product_id] = barcode
        shelf_id2barcodes = {}
        for shelf_id in shelf_ids:
            shelf_barcodes = self.planogram.obtain_barcodes_by_loc(gondolaID.gondola_id,shelf_id)
            shelf_id2barcodes[shelf_id] = shelf_barcodes
        print(shelf_id2barcodes)

        # obtain similarity between merged cluster to shelf

        for merged_id, product_ids in merged_id2product_id.items():
            for shelf_id, barcodes in shelf_id2barcodes.items():
                sim_count = 0
                total_count = 0
                for product_id in product_ids:
                    vision_pred = product_id2vision_pred[product_id]
                    if vision_pred in barcodes:
                        sim_count += 1
                    total_count += 1
                merged2shelf_sim_classifier[merged_id, shelf_id-1] = sim_count/total_count # notice shelf_id starts with 1
        return merged2shelf_sim_classifier

    def infer_shelf_lines_by_matched_shelves(self, matched_shelf_id2shelf_line, 
                                        unmatched_merge_ids2center, 
                                        shelf_id2gap):
        ''' infer the all shelf_lines using shelf height and matched shelves_lines,
            merge inferred shelf_lines that are too close to unmatched clusters.
        '''
        shelf_id2height = {}
        for shelf_id in shelf_id2gap.keys():
            if shelf_id not in matched_shelf_id2shelf_line.keys():

                # this is the code to use all matched shelf to infer unmatched shelf, and use the average
                # infered_shelf_lines = []
                # for matched_shelf_id, matched_shelf_line in matched_shelf_id2shelf_line.items():
                #     infered_shelf_lines.append(self.infer_shelf_line_by_shelf_height(shelf_id, matched_shelf_id, shelf_id2gap, matched_shelf_line))
                # inferred_shelf_line = sum(infered_shelf_lines)/len(infered_shelf_lines) # use averged value

                # this is the code to use cloest matched shelf to infer unmatched shelf, and use the average
                matched_shelf_ids_list = list(matched_shelf_id2shelf_line.keys())
                diff_list = np.array([abs(i - shelf_id) for i in matched_shelf_ids_list])
                closest_ids = list(np.where(diff_list == diff_list.min())[0])
                print(closest_ids)
                infered_shelf_lines = []
                for i in closest_ids:
                    anchor_shelf_id = matched_shelf_ids_list[i]
                    print(anchor_shelf_id)
                    infered_shelf_lines.append(self.infer_shelf_line_by_shelf_height(shelf_id, anchor_shelf_id, shelf_id2gap, matched_shelf_id2shelf_line[anchor_shelf_id]))
                inferred_shelf_line = sum(infered_shelf_lines)/len(infered_shelf_lines) # use averged value

                # this is the code to match newly obtain shelfline with previou unmatch cluster
                shelf_line = inferred_shelf_line
                for unmatched_merge, height in unmatched_merge_ids2center.items():
                    if abs(height - inferred_shelf_line) < 0.5 * shelf_id2gap[shelf_id]:
                        print(f"match inferred shelf {shelf_id} with unmatched merged cluster {unmatched_merge}")
                        shelf_line = height
                shelf_id2height[shelf_id] = shelf_line
            else:
                shelf_id2height[shelf_id] = matched_shelf_id2shelf_line[shelf_id]
        return shelf_id2height 
    
    def infer_shelf_line_by_shelf_height(self, shelf1, shelf2, shelf_id2height, shelf2_line):
        ''' shelf1, the one to be inferred, shelf2, the one used as anchor'''
        print("shelf_id2height", shelf_id2height)
        print(f'infer shelf {shelf1} using shelf {shelf2} as anchor')
        lower_shelf, upper_shelf, reverse = (shelf1, shelf2, True) if shelf1 < shelf2 else (shelf2, shelf1, False)
        print(lower_shelf, upper_shelf, reverse)
        shelf_line_diff = 0
        for shelf_id in range(lower_shelf, upper_shelf):
            shelf_line_diff += shelf_id2height[shelf_id]
        print("shelf_line_diff 0", shelf_line_diff)
        shelf_line_diff = shelf_line_diff if not reverse else -1 * shelf_line_diff
        print("shelf_line_diff 1", shelf_line_diff)
        print("shelf2_line", shelf2_line)
        shelf1_line = shelf2_line + shelf_line_diff
        print("shelf1_line", shelf1_line)
        return shelf1_line

    def obtain_shelf_line(self, camera_view_query, gondolaID, merged_id2merged_center, product_id2merged_id, merged_id2product_id):
        total_merges = len(merged_id2merged_center.keys())

        # obtain valid shelf_id
        shelf_ids = self.planogram.obtain_shelf_ids_from_gondola(gondolaID.gondola_id)
        print(shelf_ids)
        total_shelves = len(shelf_ids)

        if total_merges == total_shelves:
            print("merge shelf match")
            shelf_id2shelf_line = {}
            for merged_id, merged_center in merged_id2merged_center.items():
                shelf_id2shelf_line[merged_id + 1] = merged_center # merged_id start from 0, shelf_id start from 1
        else:
            # print(total_merges, total_shelves)
            print("merge less than shelf, use vision_classifier to match")

            matched_shelf_id2shelf_line, unmatched_merge_ids2center = self.match_merged_to_shelf(camera_view_query, 
                        merged_id2merged_center, gondolaID, product_id2merged_id, merged_id2product_id, shelf_ids)

            shelf_id2height = self.obtain_shelf_height(gondolaID.gondola_id)

            # use higher matched shelf to locate lower unmatched shelf, infer with shelf height, use merged cluster if close enough
            shelf_id2shelf_line = self.infer_shelf_lines_by_matched_shelves(matched_shelf_id2shelf_line, unmatched_merge_ids2center, shelf_id2height)

        print("shelf_id2shelf_line", shelf_id2shelf_line)
        return shelf_id2shelf_line

    def infer_shelf_id_by_shelf_line(self, camera_view_query, gondolaID, shelf_id2shelf_line):
        shelf_ids, shelf_lines = [], []
        for shelf_id, shelf_line in shelf_id2shelf_line.items():
            shelf_ids.append(shelf_id)
            shelf_lines.append(shelf_line)
        shelf_lines = np.array(shelf_lines)
        product_id2shelf_id = {}
        for productquery in camera_view_query.productqueries:
            if productquery.pred_gondolaID == gondolaID:
                assert productquery.dist_to_bottom is not None
                dist2shelf_line = np.abs(shelf_lines - productquery.dist_to_bottom)
                shelf_id = shelf_ids[np.argmin(dist2shelf_line)]
                productquery.pred_shelfID = ShelfId(gondolaID.gondola_id, shelf_id)
                product_id2shelf_id[productquery.product_id] = shelf_id
        return product_id2shelf_id

    ##########################################################
    ############ major function to process bins ##############
    ##########################################################

    def obtain_products_bin_id_barcodes_by_shelf_ID(self, camera_view_query:CameraViewQuery, shelf_ID:ShelfId):
        product_ids_and_dist_to_left = []
        for product_query in camera_view_query.productqueries:
            if product_query.pred_shelfID == shelf_ID: # only process image of corresponding shelf
                product_ids_and_dist_to_left.append([product_query.product_id, product_query.dist_to_left])
        
        def sort_func(id_and_dist_to_left):
            return id_and_dist_to_left[1]
        
        product_ids_and_dist_to_left.sort(key=sort_func)

        print("product_ids_and_dist_to_left", product_ids_and_dist_to_left)

        product_ids = [product_id for product_id, _ in product_ids_and_dist_to_left]
        
        gondola_id, shelf_id  = shelf_ID.gondola_id, shelf_ID.shelf_index
        barcodes, locs = self.planogram.obtain_barcodes_by_loc(gondola_id, shelf_id, return_loc=True)
        bin_ids = [loc[2] for loc in locs]
        print("product_ids", product_ids)
        return product_ids, bin_ids, barcodes

    @staticmethod
    def check_products_bin_sim_matrix(sim_matrix):
        pass_ = False
        if sim_matrix.size == 0:
            print("empty sim matrix, no product located! dont process!")
        else:
            total_bins, total_products = sim_matrix.shape
            if total_bins - 3 > total_products:
                print("total products are three or more less than total bins! dont process")
            else:
                pass_ = True
        return pass_


    def obtain_product2bin_similarity_by_classifier(self, product_ids, bin_ids, barcodes, camera_view_query):
        classes = self.vision_classifier.obtain_classes_of_barcodes(barcodes)
        sim_matrix = np.zeros((len(bin_ids), len(product_ids)))
        for i, product_id in enumerate(product_ids):
            image = camera_view_query.productqueries[product_id].image
            _, prob, _ = self.vision_classifier.inference(image)
            for j, class_ in enumerate(classes):
                if class_ != -1:
                    prob_ = prob[class_]
                else:
                    prob_ = - 1
                sim_matrix[j, i] = prob_
        return sim_matrix

    def obtain_product2bin_similarity_by_bank_and_planogram(self, product_ids, bin_ids, barcodes, camera_view_query):
        sim_matrix = np.zeros((len(bin_ids), len(product_ids)))
        barcode2source = {}
        for i, product_id in enumerate(product_ids):
            image = camera_view_query.productqueries[product_id].image
            for j, barcode in enumerate(barcodes):
                if barcode in self.vision_classifier.barcode2class.keys():
                    sim = self.vision_classifier.feature_similarity_with_barcode_by_image_bank([image], barcode)[0]
                    barcode2source[barcode] = "feature_bank"
                else:
                    sim = self.vision_classifier.feature_similarity_with_barcode_by_planogram([image], barcode)[0]
                    barcode2source[barcode] = "planogram"
                sim_matrix[j, i] = sim
        return sim_matrix, barcode2source

    def obtain_product2bin_similarity_by_planogram(self, product_ids, bin_ids, barcodes, camera_view_query):
        sim_matrix = np.zeros((len(bin_ids), len(product_ids)))
        barcode2source = {}
        for i, product_id in enumerate(product_ids):
            image = camera_view_query.productqueries[product_id].image
            for j, barcode in enumerate(barcodes):
                sim = self.vision_classifier.feature_similarity_with_barcode_by_planogram([image], barcode)[0]
                barcode2source[barcode] = "planogram"
                sim_matrix[j, i] = sim
        exp_sim = np.exp(sim_matrix)
        sim_matrix = exp_sim/exp_sim.sum(axis=0, keepdims=True)
        return sim_matrix, barcode2source


    def obtain_pair_wise_similarity(self, product_ids, camera_view_query, use_filtered = False):
        features = []
        for i, product_id in enumerate(product_ids):
            if use_filtered:
                image = camera_view_query.productqueries[product_id].filtered_image
            else:
                image = camera_view_query.productqueries[product_id].image
            feature = self.vision_classifier.obtain_feature(image).numpy()
            feature /= np.linalg.norm(feature, keepdims=True)
            features.append(feature)
        pair_wise_sim = []
        for i in range(len(features) -1):
            feature_l = features[i]
            feature_r = features[i+1]
            sim = (feature_l * feature_r).sum()
            pair_wise_sim.append(sim)
        return pair_wise_sim

    def inference_products2bins(self, sim_maxtrix):
        dtw_path, _, cost_matrix = self.dtw_predictor.predict(sim_maxtrix)
        dtw_path = self.dtw_predictor.remove_outside_gondola_products(sim_maxtrix, dtw_path)

        if dtw_path:
            sims = []
            for step in dtw_path:
                bin, product = step
                sims.append(sim_maxtrix[bin, product])
            min_sim = min(sims)
            avg_sim = sum(sims)/len(dtw_path)
        else:
            min_sim = -1
            avg_sim = -1

        infer_info = f"average sim {avg_sim}, min sim {min_sim}"

        return dtw_path, avg_sim, min_sim, cost_matrix, infer_info

    def inference_bin_line_by_path(self, camera_view_query:CameraViewQuery, shelf_ID:ShelfId, product_ids, bin_ids, path, method, shelf_line = None):
        assert method in ["projection", "2d_mid"]
        if method == "projection":
            assert shelf_line is not None

        bin_id2product_ids = {bin_id:[] for bin_id in bin_ids}
        for bin_id in bin_ids:
            for step in path:
                if step[0] == (bin_id - 1):
                    bin_id2product_ids[bin_id].append(product_ids[step[1]])
        shelf_id = shelf_ID.shelf_index

        height, width, _ = camera_view_query.image.shape
        camera_id = camera_view_query.camera_id
        camera = self.cameras[camera_id]
        gondolaID = GondolaId(shelf_ID.gondola_id)
        gondola_front_plane = self.store.front_planes[gondolaID]
        gondolabox = self.store.bounding_boxes[gondolaID]
        p0, p1, p6, p7 = gondolabox.corners[0], gondolabox.corners[1], gondolabox.corners[6], gondolabox.corners[7]
        _,gondola_width = obtain_vector_3d(p0, p7)
        vec01, vec01_length = obtain_vector_3d(p0, p1)
        vec76, vec76_length = obtain_vector_3d(p7, p6)

        # obtain the left and right boundry of each bins seperately in 2d
        bin_id2left_right = {}
        if method == "2d_mid":
            for bin_id, product_ids in bin_id2product_ids.items():
                if len(product_ids) != 0:
                    left_most_product = camera_view_query.productqueries[product_ids[0]]
                    right_most_product = camera_view_query.productqueries[product_ids[-1]]
                    x,y,w,h = left_most_product.bbox
                    left_mid2d = np.array([x/width, (y + h/2)/height])
                    x,y,w,h = right_most_product.bbox
                    right_mid2d = np.array([(x+w)/width, (y + h/2)/height])
                    left_right = (left_mid2d, right_mid2d)
                else:
                    left_right = None
                bin_id2left_right[bin_id] = left_right
        elif method == "projection":
            psl = p0 + vec01 * shelf_line / vec01_length # shelf line left point
            psr = p7 + vec76 * shelf_line / vec76_length # shelf line right point
            shelf_points2d = self.cameras[camera_view_query.camera_id].project(np.array([psl, psr]))
            shelf_points2d *= np.array([width, height])
            shelf_points2d = list(shelf_points2d)
            for bin_id, product_ids in bin_id2product_ids.items():
                if len(product_ids) != 0:
                    left_most_product = camera_view_query.productqueries[product_ids[0]]
                    right_most_product = camera_view_query.productqueries[product_ids[-1]]
                    x,y,w,h = left_most_product.bbox
                    corners = [[x, y], [x + w, y], [x, y + h], [x+w, y +h]]
                    projected_x, projected_corners = [], []
                    for corner in corners:
                        corner = np.array(corner)
                        projected_corner = obtain_projection2line2d(corner, shelf_points2d)
                        projected_x.append(projected_corner[0])
                        projected_corners.append(projected_corner)
                    left_id = np.argmin(np.array(projected_x))
                    left = projected_corners[left_id]/np.array([width, height])

                    x,y,w,h = right_most_product.bbox
                    corners = [[x, y], [x + w, y], [x, y + h], [x+w, y +h]]
                    projected_x = []
                    for corner in corners:
                        corner = np.array(corner)
                        projected_corner = obtain_projection2line2d(corner, shelf_points2d)
                        projected_x.append(projected_corner[0])
                    right_id = np.argmin(np.array(projected_x))
                    right = projected_corners[right_id]/np.array([width, height])

                    left_right = (left, right)
                else:
                    left_right = None
                bin_id2left_right[bin_id] = left_right

        # obtain the bin line in 3d

        bin_ids = list(bin_id2left_right.keys())
        bin_id2bin_line = {}
        last_bin_line = None

        def find_next_available_bin(bin_id, bin_id2left_right):
            next_bin_id = None
            for curr_bin_id, left_right in bin_id2left_right.items():
                if (curr_bin_id > bin_id) and (left_right is not None):
                    next_bin_id = curr_bin_id
                    break
            print("bin_id", bin_id, "next_avail_bin_id", next_bin_id)
            return next_bin_id
        
        def assign_bin_line(bin_id, bin_line, gondola_width, bin_id2bin_line, assigned_bin_ids):
            bin_line = bin_line if bin_line >0 else 0
            bin_line = bin_line if bin_line < gondola_width else gondola_width
            bin_id2bin_line[bin_id] = bin_line
            assigned_bin_ids.append(bin_id)
            print("assign bin line", bin_line, "to bin id", bin_id)
            return bin_id2bin_line, assigned_bin_ids
        
        print("bin_id2left_right", bin_id2left_right)

        assigned_bin_ids = []
        last_bin_line = 0
        min_gap = 0.001
        print("bin ids", bin_ids)
        for curr_bin_id in bin_ids: # the last bin has line by gondola
            print("current bin id", curr_bin_id)
            if curr_bin_id != max(bin_ids) and curr_bin_id not in assigned_bin_ids:
                curr_left_right = bin_id2left_right[curr_bin_id]
                if curr_left_right is None:
                    bin_line = last_bin_line + min_gap
                    bin_id2bin_line, assigned_bin_ids = assign_bin_line(curr_bin_id, bin_line, gondola_width, bin_id2bin_line, assigned_bin_ids)
                else:
                    curr_right = curr_left_right[1]
                    next_bin_id = find_next_available_bin(curr_bin_id, bin_id2left_right)
                    if next_bin_id is not None:
                        next_left_right = bin_id2left_right[next_bin_id]
                        next_left = next_left_right[0]
                        mid_point = (curr_right + next_left)/2
                        mid_point = np.expand_dims(mid_point, axis=0)
                        mid_point = camera.back_project_to_planes(mid_point, gondola_front_plane)[0]
                        mid_point = obtain_distance2line3d(mid_point, (p0, p1))
                        for i, bin_id in enumerate(range(curr_bin_id, next_bin_id)):
                            bin_line = mid_point + min_gap * i
                            bin_id2bin_line, assigned_bin_ids = assign_bin_line(bin_id, bin_line, gondola_width, bin_id2bin_line, assigned_bin_ids)
                    else:
                        curr_right = np.expand_dims(curr_right, axis=0)
                        curr_right = camera.back_project_to_planes(curr_right, gondola_front_plane)[0]
                        bin_line = obtain_distance2line3d(curr_right, (p0, p1))
                        bin_id2bin_line, assigned_bin_ids = assign_bin_line(bin_id, bin_line, gondola_width, bin_id2bin_line, assigned_bin_ids)
            elif curr_bin_id == max(bin_ids):
                bin_line = gondola_width
                bin_id2bin_line, assigned_bin_ids = assign_bin_line(curr_bin_id, bin_line, gondola_width, bin_id2bin_line, assigned_bin_ids)
            else:
                pass # already assigned
            last_bin_line = bin_id2bin_line[max(assigned_bin_ids)]
        print(bin_id2bin_line)
        return bin_id2bin_line

    def detect_shelf_anomaly(self, path, sim_matrix):
        not_predicted = True if len(path) == 0 else False
        print("path", path)
        print("not_predicted", not_predicted)

        if not not_predicted:
            total_bins, total_products = sim_matrix.shape
            all_bins = set(list(range(total_bins)))
            existed_bin = set([bin_id for bin_id, _ in path])
            print("existed_bin", existed_bin)
            empyt_bin = False if set(existed_bin) == set(all_bins) else True

            wrong_seq = False
            last_bin_id = -1
            for bin_id, _ in path:
                if bin_id < last_bin_id:
                    wrong_seq = True

            if path[0][1] != 0 or path[-1][1] != (total_products - 1):
                wrong_gondola = True
            else:
                wrong_gondola = False
        else:
            empyt_bin = True
            wrong_seq = True
            wrong_gondola = True

        return not_predicted, empyt_bin, wrong_seq, wrong_gondola

    def obtain_dist2bottom_dist2left(self, camera_view_query):
        camera_id = camera_view_query.camera_id
        for gondola_id in self.camera_id2gondola_id[camera_id]:
            gondolaID = GondolaId(gondola_id)
            gondola_front_plane = self.store.front_planes[gondolaID]
            gondolabox = self.store.bounding_boxes[gondolaID]
            p0, p7, p1 = gondolabox.corners[0], gondolabox.corners[7], gondolabox.corners[1] # front_bottom_left, front_bottom_right, front_upper_left
            image = camera_view_query.image.copy() # avoid change camera_view_query
            height, width, _ = image.shape
            camera = self.cameras[camera_id]
            for productquery in camera_view_query.productqueries:
                if productquery.pred_gondolaID == gondolaID:
                    x,y,w,h = productquery.bbox
                    if self.use_bottom_point:
                        cx = (x + w*0.5)/width
                        cy = (y + h)/height
                    else:
                        cx = (x + w*0.5)/width
                        cy = (y + h*0.5)/height
                    cxcy3d = camera.back_project_to_planes(np.array([[cx, cy]]), gondola_front_plane)
                    dist2bottom = obtain_distance2line3d(cxcy3d[0], (p0,p7))
                    productquery.dist_to_bottom = dist2bottom
                    dist2left = obtain_distance2line3d(cxcy3d[0], (p0,p1))
                    productquery.dist_to_left = dist2left

    ###########################################
    ############# other functions #############
    ###########################################

    def obtain_shelf_height(self, gondola_id):
        shelvesIDs = []
        for shelveID in self.store.shelves.keys():
            if shelveID.gondola_id  == gondola_id:
                shelvesIDs.append(shelveID)
        shelf_id2height = {}
        for shelveID in shelvesIDs:
            shelf_box = self.store.bounding_boxes[shelveID]
            height = shelf_box.height
            shelf_id2height[shelveID.shelf_index] = height
        print("shelf_id2height", shelf_id2height)
        return shelf_id2height

    def obtain_projected_gondola_contours(self, camear_view_query, gondolaID):
        image = camear_view_query.image
        facecontour = self.cameraId2gondolaId2facecontour[camear_view_query.camera_id][gondolaID]
        height, width, _ = image.shape
        scaled_contour = facecontour*np.array([width, height]).astype(int)
        return scaled_contour

    def debug_print(self, content):
        if self.debug:
            print(content)
    
    def shelf_line_2_shelf_line_ends(self, shelf_line, gondolabox):
        p0, p7 = gondolabox.corners[0], gondolabox.corners[7] # front_bottom_left, front_bottom_right
        p1, p6 = gondolabox.corners[1], gondolabox.corners[6] # front_top_left, front_top_right
        vec01, vec01_length = obtain_vector_3d(p0, p1)
        vec76, vec76_length = obtain_vector_3d(p7, p6)
        psl = p0 + vec01 * shelf_line / vec01_length # shelf line left point
        psr = p7 + vec76 * shelf_line / vec76_length # shelf line right point
        return (psl, psr)

    ##############################################
    ############### visualization ################
    ##############################################

    def plot_box_on_camera_view(self, camera_id, dest_dir, camera_view_query = None, box_type = "gondola"):
        if camera_view_query is not None:
            assert camera_view_query is CameraViewQuery
            assert camera_id == camera_view_query.camera_id
            image = camera_view_query.image
        else:
            height, width = self.camera_view_hw
            image = np.zeros([height, width, 3])
        camera = self.cameras[camera_id]
        all_boxes = self.store.bounding_boxes 
        if box_type == "gondola":
            boxIds = self.store.gondolas.keys()
        elif box_type == "shelf":
            boxIds = self.store.shelves.keys()
        elif box_type == "bin":
            boxIds = self.store.bins.keys()
        else:
            raise NotImplementedError
        
        all_target_boxes = {boxId: box for boxId, box in all_boxes.items() if boxId in boxIds}
        camera = self.cameras[camera_id]
        scale = 2.0
        image, camera = enlarge_fov(image, camera, scale=scale)
        for component_id, box in all_target_boxes.items():
            visualize_component(
                image,
                camera,
                component_box=box,
                line_style=LineStyle(GREY, 1),
                highlight=LineStyle(GREEN, 3),
                text=str(component_id.pretty()),
                text_style=TextStyle(RED),
                margin=0.5 / scale,
            )
        os.makedirs(dest_dir, exist_ok=True)
        dest_path = os.path.join(dest_dir, f"camera_{camera_id}_{box_type}_plots.jpg")
        cv2.imwrite(dest_path, image)
        
    def visualize_shelf_line_bin_line(self, camera_view_query, shelf_id2shelf_line, shelf_id2bin_id2bin_line, gondolaID, dest_dir):
        gondolabox = self.store.bounding_boxes[gondolaID]
        p0, p7 = gondolabox.corners[0], gondolabox.corners[7] # front_bottom_left, front_bottom_right
        p1, p6 = gondolabox.corners[1], gondolabox.corners[6] # front_top_left, front_top_right
        vec01, vec01_length = obtain_vector_3d(p0, p1)
        vec76, vec76_length = obtain_vector_3d(p7, p6)
        vec07, vec07_length = obtain_vector_3d(p0, p7)
        colors_shelf = obtain_colors_from_color_map(len(shelf_id2shelf_line.keys()))
        
        image = camera_view_query.image.copy()
        for i, (shelf_id, shelf_line) in enumerate(shelf_id2shelf_line.items()):
            psl = p0 + vec01 * shelf_line / vec01_length # shelf line left point
            psr = p7 + vec76 * shelf_line / vec76_length # shelf line right point
            points = obtain_points_on_lineseg_3d(psl, psr, 5) # obtain points on the shelf line
            points = np.array(points)
            points2d = self.cameras[camera_view_query.camera_id].project(points)
            height, width, _ = image.shape
            points2d *= np.array([width, height])
            points2d = list(points2d)
            points2d = [[int(cordx), int(cordy)] for cordx, cordy in points2d]
            color = colors_shelf[i]
            for point0, point1 in zip(points2d[:-1], points2d[1:]):
                cv2.line(image, point0, point1, color, 2)
            if shelf_id in shelf_id2bin_id2bin_line.keys():
                bin_id2bin_line = shelf_id2bin_id2bin_line[shelf_id]
                colors_bin = obtain_colors_from_color_map(len(bin_id2bin_line.keys()))
                for j, (_, bin_line) in enumerate(bin_id2bin_line.items()):
                    print(j, bin_line)
                    if bin_line is not None:
                        bin_color = colors_bin[j]
                        if shelf_id != max(list(shelf_id2shelf_line.keys())):
                            next_shelf_line = shelf_id2shelf_line[shelf_id + 1]
                        else:
                            next_shelf_line = vec01_length
                        pbb = p0 + vec01 * shelf_line / vec01_length + vec07 * bin_line / vec07_length
                        pbt = p0 + vec01 * next_shelf_line / vec01_length + vec07 * bin_line / vec07_length
                        points = obtain_points_on_lineseg_3d(pbb, pbt, 2)
                        points2d = self.cameras[camera_view_query.camera_id].project(points)
                        points2d *= np.array([width, height])
                        points2d = [[int(cordx), int(cordy)] for cordx, cordy in points2d]
                        print(points2d)
                        for point0, point1 in zip(points2d[:-1], points2d[1:]):
                            print(point0, point1)
                            cv2.line(image, point0, point1, bin_color, 2)
        dest_path = os.path.join(dest_dir, f"camera_{camera_view_query.camera_id}_gondola_{gondolaID.gondola_id}_shelf_bin_line.jpg")
        cv2.imwrite(dest_path, image)

    def visualize_product2shelf(self, camera_view_query:CameraViewQuery, gondolaID:GondolaId, dest_dir):
        image = camera_view_query.image.copy()
        height,width,_ = image.shape
        num_of_shelfs = len(self.planogram.obtain_shelf_ids_from_gondola(gondolaID.gondola_id))
        colors = obtain_colors_from_color_map(num_of_shelfs)
        for productquery in camera_view_query.productqueries:
            if productquery.pred_gondolaID == gondolaID:
                pred_shelf_id = productquery.pred_shelfID.shelf_index
                color = colors[pred_shelf_id-1]
                x,y,w,h = productquery.bbox
                xw = x+w if x+w<width else width
                yh = y+h if y+h<height else height
                image = cv2.rectangle(image, (x,y), (xw,yh), color, thickness=2)
        os.makedirs(dest_dir, exist_ok=True)
        dest_path = os.path.join(dest_dir, f"camera_{camera_view_query.camera_id}_gondola_{gondolaID.gondola_id}_product2shelf.jpg")
        cv2.imwrite(dest_path, image)

    def obtain_camera_id2gondola_id(self, camera_maps):
        if camera_maps:
            camera_id2gondola_id = {}
            for gondola, cameras in camera_maps.items():
                for camera in cameras:
                    if camera in camera_id2gondola_id.keys():
                        camera_id2gondola_id[camera].append(gondola)
                    else:
                        camera_id2gondola_id[camera] = [gondola]
        else:
            camera_id2gondola_id = None
        return camera_id2gondola_id

    def visualize_bin_analysis(self, camera_view_query:CameraViewQuery, shelfID:ShelfId, sim_matrix, cost_matrix, dest_dir:str, path:List, pair_wise_sim = None, inference_info = "None", barcode2source = None):

        print("length of pair wise sim", len(pair_wise_sim))

        gondola_id, shelf_id = shelfID.gondola_id, shelfID.shelf_index
        camera_id = camera_view_query.camera_id
        current_dest_dir = os.path.join(dest_dir, "dtw for bins")
        os.makedirs(current_dest_dir, exist_ok=True)

        ########## plot image of all products on the shelf #########
        xs, ys, xws, yhs = [], [], [], []
        camera_image = camera_view_query.image.copy()
        height, width, _ = camera_image.shape
        product_x_image_id = []

        for productquery in camera_view_query.productqueries:
            if productquery.pred_shelfID == shelfID:
                product_id = productquery.product_id
                x,y,w,h = productquery.bbox
                xw = x+w if x+w<width else width
                yh = y+h if y+h<height else height
                product_image = camera_view_query.image.copy()[y:yh, x:xw, :]
                id_image = np.ones((40,70, 3))*255
                id_image = cv2.putText(id_image, str(product_id), org = (10,30), fontFace = cv2.FONT_HERSHEY_SIMPLEX,fontScale = 0.7, color = (255,0,0), thickness=1)
                product_image = join_images([product_image, id_image], gap = 0, direction="vertical")
                product_x_image_id.append([x, product_image, product_id])
                xs.append(x)
                ys.append(y)
                xws.append(xw)
                yhs.append(yh)
                camera_image = cv2.rectangle(camera_image, (x,y), (xw,yh), (0,255,0), thickness=1)
                

        x,y,xw,yh = min(xs), min(ys), max(xws), max(yhs)
        x = max(x - 20, 0)
        y = max(y - 20, 0)
        xw = min(xw + 20, width)
        yh = min(yh + 20, height) 
        products_image = camera_image[int(y): int(yh), int(x): int(xw), :]

        def sort_func(product_x_and_image):
            return product_x_and_image[0]
        
        product_x_image_id.sort(key=sort_func)

        product_ids = [product_id for _, _, product_id  in product_x_image_id]
        total_products = len(product_ids)
        
        product_1_by_1_images = [product_image for _, product_image, _  in product_x_image_id]
        
        if pair_wise_sim is not None:
            for i, product_image in enumerate(product_1_by_1_images):
                if i != len(pair_wise_sim):
                    pair_wise_sim_image = np.ones((50, 10, 3)) * 255
                    sim = pair_wise_sim[i]
                    pair_wise_sim_image = cv2.line(pair_wise_sim_image, (5,50), (5, 50 - int(50*sim)), color = (255,0,0), thickness= 2)
                    pair_wise_sim_image = cv2.line(pair_wise_sim_image, (5,50 - int(50*sim)), (5, 0), color = (0,0,255), thickness= 2)
                    product_image = join_images([product_image, pair_wise_sim_image], gap = 0, direction="horizontal")
                    product_1_by_1_images[i] = product_image
        
        product_1_by_1_image = join_images(product_1_by_1_images, gap=20, direction="horizontal")

        ########### plot images of bins from planogram #############
        barcodes = self.planogram.obtain_barcodes_by_loc(shelfID.gondola_id, shelfID.shelf_index)
        total_bins = len(barcodes)
        height = 200
        width = 200
        gap = 50
        planogram_images = []
        total_width = 0
        for barcode in barcodes: # barcodes are sorted by bins in self.obtain_barcodes_by_loc
            planogram_image_path = self.planogram.barcode2image_path[barcode]
            if planogram_image_path is not None:
                planogram_image = cv2.imread(planogram_image_path)
            else:
                planogram_image = np.ones((height,width,3)) * 127
            planogram_image = rescale_image_fix_ratio(planogram_image, (height, width))
            _, img_width, _ = planogram_image.shape
            planogram_images.append(planogram_image)
            total_width += (img_width + gap)

        planogram_shelf_image = join_images(planogram_images, gap, "horizontal")

        ########### plot images of sim matrix from planogram #############
        total_bins, total_products = sim_matrix.shape
        fig, ax = plt.subplots()
        _ = ax.imshow(sim_matrix, vmin = -1, vmax = 1)

        ax.set_xticks(np.arange(total_products), labels = product_ids, size = "x-small")
        ax.set_yticks(np.arange(total_bins), labels = barcodes)
        
        for i in range(total_bins):
            for j in range(total_products):
                if path:
                    if (i,j) in path:
                        color = "r"  
                    else:
                        color = "w"
                else:
                    color = "w"
                _ = ax.text(j, i, "{0:.2f}".format(sim_matrix[i, j]), ha="center", va = "center", color = color, size = "x-small")
        
        if barcode2source is not None:
            for ticklabel, barcode in zip(plt.gca().get_yticklabels(), barcodes):
                source = barcode2source[barcode]
                if source == "planogram":
                    tickcolor = "r"
                elif source == "feature_bank":
                    tickcolor = "g"
                elif source == "classifier":
                    tickcolor == "b"
                else:
                    raise ValueError
                ticklabel.set_color(tickcolor)
        ax.set_title("similarity_matrix_x_products_y_bins")

        ax.set_xticks(np.arange(-.5, total_products, 1), minor=True)
        ax.set_yticks(np.arange(-.5, total_bins, 1), minor=True)

        ax.grid(which='minor', color='k', linestyle='-', linewidth=1)
        fig.tight_layout()

        sim_matrix_image_path = os.path.join(current_dest_dir, "sim_matrix.jpg")
        plt.savefig(sim_matrix_image_path, dpi = 200)
        plt.close()

        ############## plot cost matrix for analyse ################
        N, M = cost_matrix.shape
        fig, ax = plt.subplots()
        _ = ax.imshow(cost_matrix, vmin = -1, vmax = 1)

        print('----')
        print(N, M)
        print(total_products)
        print(total_bins)
        ax.set_xticks(np.arange(M), labels = [""] + product_ids + [""], size = "x-small")

        ax.set_yticks(np.arange(N), labels = ["" for i in range(self.dtw_predictor.max_skip)] + [""] + 
                                                barcodes 
                                                + ["" for i in range(self.dtw_predictor.max_reverse)] + [""])
        
        for i in range(N):
            for j in range(M):
                if path:
                    if (i - self.dtw_predictor.max_skip -1, j-1) in path:
                        color = "r"  
                    else:
                        color = "w"
                else:
                    color = "k"
                _ = ax.text(j, i, "{0:.2f}".format(cost_matrix[i, j]), ha="center", va = "center", color = color, size = "x-small")

        ax.set_title("cost_matrix_x_products_y_bins")

        ax.set_xticks(np.arange(-.5, M, 1), minor=True)
        ax.set_yticks(np.arange(-.5, N, 1), minor=True)
        ax.grid(which='minor', color='k', linestyle='-', linewidth=1)
        
        fig.tight_layout()
        cost_matrix_image_path = os.path.join(current_dest_dir, "cost_matrix.jpg")
        plt.savefig(cost_matrix_image_path, dpi = 200)
        plt.close()

        ########### combine images ###############
        gap = 50
        sim_matrix_image = cv2.imread(sim_matrix_image_path)
        os.remove(sim_matrix_image_path) # remove, no longer needed

        cost_matrix_image = cv2.imread(cost_matrix_image_path)
        os.remove(cost_matrix_image_path)
        
        # plot an image to print info
        info_image = np.ones((100, 1500, 3)) * 255 
        cv2.putText(info_image, inference_info, (30,30), fontFace = cv2.FONT_HERSHEY_SIMPLEX, fontScale = 1, color = (0,0,0), thickness=2)

        # plot an image to print note
        note_image = np.ones((100, 1500, 3)) * 255 
        note = "barcode: green:dataset, red:planogram, blue:classifier; matrix value, red: matched pair"
        cv2.putText(note_image, note, (30,30), fontFace = cv2.FONT_HERSHEY_SIMPLEX, fontScale = 1, color = (0,0,0), thickness=2)

        combined_image = join_images([products_image, product_1_by_1_image, planogram_shelf_image, sim_matrix_image, cost_matrix_image, info_image, note_image], gap, direction="vertical")

        combined_image_path = os.path.join(current_dest_dir, f"camera_{camera_id}_gondola_{gondola_id}_shelf_{shelf_id}_bin_analysis.jpg")
        cv2.imwrite(combined_image_path, combined_image)



