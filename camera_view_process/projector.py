from cProfile import label
from dataclasses import dataclass
from turtle import width
import matplotlib.pyplot as plt
from proto_tools.codec import StoreCalibration
from proto_tools.codec.components import GondolaId,ShelfId,BinId
from geometry import Camera
from geometry.utils_2d.polygon import Polygon2d
import numpy as np
import copy
from aifi_visualization.constants import BLUE, GREEN, GREY, ORANGE, RED
from aifi_visualization.utils import enlarge_fov
from typing import Collection, Dict, Iterable, List, Sequence, Tuple, Union
from aifi_visualization.visualize_components import (
    LineStyle,
    TextStyle,
    visualize_box,
    visualize_component
)
from camera_view_process.visionclassifier import VisionClassifier
import math
import os
from geometry.box import Box

import cv2
from pygments import highlight
from sklearn import cluster
from camera_view_process.data import ProductQuery, CameraViewQuery, Planogram
from camera_view_process.visionclassifier import VisionClassifier
from camera_view_process.utils import obtain_colors_from_color_map, obtain_vector_3d, obtain_points_on_lineseg_3d, obtain_distance2line3d, rescale_image_fix_ratio, obtain_projection2line2d, join_images


class LayoutProjector():
    """ Using the bounding box of all products from a camera view to locate the gondola shelf of each product 
    """
    def __init__(self, store_calibration_path: str, 
                        camera_maps: Dict[int, List[int]], 
                        vision_classifier: VisionClassifier, 
                        planogram: Planogram,
                        camera_view_hw = [1080,1920],
                        ):
        with open(store_calibration_path) as fp:
            self.store = StoreCalibration.from_json(fp.read())
        self.store.cameras = self.store.cameras.to_meters()
        self.cameras = self.store.cameras
        self.camera_maps = camera_maps
        self.camera_id2gondola_id = self.obtain_camera_id2gondola_id(camera_maps)
        self.camera_view_hw = camera_view_hw
        self.vision_classifier = vision_classifier
        self.planogram = planogram
    
    def obtain_camera_id2gondola_id(self, camera_maps):
        camera_id2gondola_id = {}
        for gondola, cameras in camera_maps.items():
            for camera in cameras:
                if camera in camera_id2gondola_id.keys():
                    camera_id2gondola_id[camera].append(gondola)
                else:
                    camera_id2gondola_id[camera] = [gondola]
        return camera_id2gondola_id


    def obtain_polygon_of_a_box(self, box, camera_id):
        height, width = self.camera_view_hw
        camera = self.cameras[camera_id]
        visible = np.any(
            camera.are_points_visible(box.corners, margin=0.0)
        )
        facing_camera = (
            np.dot(camera.forward_vector, box.forward_vector) < 0.0
        )
        valid = visible and facing_camera
        if valid:
            base_polygon  = Polygon2d([np.array([0,0]),
                            np.array([width, 0]),
                            np.array([width, height]),
                            np.array([0, height])])
            face_contour_3d = box.get_face_contour(n=5) 
            face_contour_2d = camera.project_polygon(face_contour_3d)
            face_contour_2d_scaled = face_contour_2d * np.array([width, height])
            polygon = Polygon2d(face_contour_2d_scaled)
            polygon_ = polygon.intersect(base_polygon)
            assert len(polygon_)
            polygon = polygon_[0]
        else:
            polygon = None
        return polygon

    def off_set_origin_of_a_box(self, box, off_set):
        forward_vector = box.forward_vector
        neworigin = box.origin - off_set * forward_vector
        new_box = Box(neworigin, box.rotation, box.dimensions)
        return new_box

    def rotate_a_box(self, box, rotate):
        rotation = box.rotation
        new_rotation = box.rotation + rotate
        new_box = Box(box.origin, new_rotation, box.dimensions)
        return new_box

    ################ visualization ###################

    def plot_box_on_camera_view(self, camera_id, dest_dir, camera_view_query, boxs, boxIds, info):
        if camera_view_query is not None:
            assert isinstance(camera_view_query, CameraViewQuery)
            assert camera_id == camera_view_query.camera_id
            image = camera_view_query.image
        else:
            height, width = self.camera_view_hw
            image = np.zeros([height, width, 3])

        assert len(boxs) == len(boxIds)
        camera = self.cameras[camera_id]
        
        total_boxs = len(boxs)
        colors = obtain_colors_from_color_map(total_boxs)

        camera = self.cameras[camera_id]
        scale = 2.0
        image, camera = enlarge_fov(image, camera, scale=scale)
        for i, (boxId, box) in enumerate(zip(boxIds, boxs)):
            print(box)
            visualize_component(
                image,
                camera,
                component_box=box,
                line_style=None,
                highlight=LineStyle(colors[i], 1),
                text="",
                text_style=TextStyle(RED),
                margin=0.5 / scale,
            )
        os.makedirs(dest_dir, exist_ok=True)
        dest_path = os.path.join(dest_dir, info)
        cv2.imwrite(dest_path, image)

    def plot_bottom_line_of_shelve(self, camera_id, shelf_box, color, image):
        p0, p7 = shelf_box.corners[0], shelf_box.corners[7]
        points = obtain_points_on_lineseg_3d(p0, p7, 5)
        points = np.array(points)
        points2d = self.cameras[camera_id].project(points)
        height, width, _ = image.shape
        points2d *= np.array([width, height])
        points2d = list(points2d)
        points2d = [[int(cordx), int(cordy)] for cordx, cordy in points2d]
        for point0, point1 in zip(points2d[:-1], points2d[1:]):
            cv2.line(image, point0, point1, color, 1)
        return image

    def project_shelf_line_ends_to_2d_points(self, shelf_line_ends, camera_view_query):
        camera_id = camera_view_query.camera_id
        image = camera_view_query.image.copy()
        camera = self.cameras[camera_id]
        psl, psr = shelf_line_ends
        points = obtain_points_on_lineseg_3d(psl, psr, 5) # obtain points on the shelf line
        points = np.array(points)
        points2d = self.cameras[camera_view_query.camera_id].project(points)
        height, width, _ = image.shape
        points2d *= np.array([width, height])
        points2d = list(points2d)
        points2d = [[int(cordx), int(cordy)] for cordx, cordy in points2d]

        for point0, point1 in zip(points2d[:-1], points2d[1:]):
            cv2.Line