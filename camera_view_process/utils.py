import math
import numpy as np
import cv2
import pandas as pd

def obtain_vector_3d(pointa:np.ndarray, pointb:np.ndarray):
    ''' obtain the vector and length from pointa to pointb in 3d'''
    vector_a2b = np.array([pointb[0] - pointa[0], pointb[1] - pointa[1], pointb[2] - pointa[2]])
    length = math.sqrt(sum(vector_a2b*vector_a2b))
    return vector_a2b, length

def obtain_vector_2d(pointa:np.ndarray, pointb:np.ndarray):
    ''' obtain the vector and length from pointa to pointb in 2d'''
    vector_a2b = np.array([pointb[0] - pointa[0], pointb[1] - pointa[1]])
    length = math.sqrt(sum(vector_a2b*vector_a2b))
    return vector_a2b, length

def obtain_points_on_lineseg_3d(pointa, pointb, num_of_points):
    ''' obtain points on the line segment which identified by point on two ends,
        the number of points should larger than or equal to two.
    '''
    assert num_of_points >= 2 
    vector_a2b, _ = obtain_vector_3d(pointa, pointb)
    points = [pointa] # point 0 is the point a, point num_of_points - 1 is the point b
    for point_id in range(1, num_of_points -1): # 1 to num_of_points - 2
        vector_a2p = vector_a2b * point_id/(num_of_points-1)
        point = pointa + vector_a2p
        points.append(point)
    points.append(pointb)
    return points

def obtain_colors_from_color_map(num_of_colors):
    color_bar = np.array(list(range(255)), dtype="uint8").reshape((-1, 1, 1))
    color_bar = cv2.applyColorMap(color_bar, cv2.COLORMAP_HSV)
    gap = 255 // num_of_colors
    colors = []
    for i in range(0, num_of_colors):
        colors.append(color_bar[gap * i, :, :].tolist()[0])
    return colors

def obtain_distance2line3d(p, linepoints):
    p0,p1 = linepoints
    vec0p = np.array([p[0] - p0[0], p[1] - p0[1], p[2] - p0[2]])
    vec01 = np.array([p1[0] - p0[0], p1[1] - p0[1], p1[2] - p0[2]])
    projection = float((vec0p*vec01).sum()/math.sqrt((vec01*vec01).sum()))
    vec0p_length_squared = float((vec0p*vec0p).sum())
    dist_squre = vec0p_length_squared - projection**2
    if dist_squre > 0:
        return math.sqrt(dist_squre)
    else:
        return 0

def obtain_projection2line2d(p, linepoints):
    p0, p1 = linepoints
    vec01, vec01_length = obtain_vector_2d(p0, p1)
    vec0p, _ = obtain_vector_2d(p0, p)
    projection_length = float((vec0p*vec01).sum()/math.sqrt((vec01*vec01).sum()))
    print(projection_length)
    print(vec01_length)
    projection_point = p0 + vec01*projection_length/vec01_length
    return projection_point


def rescale_image_fix_ratio(image, desired_size):
    height, width = desired_size
    img_height, img_width, _ = image.shape
    img_ratio = float(img_height) / img_width
    ratio = float(height) / width
    if img_ratio > ratio:
        dest_height = height
        dest_width = int(dest_height / img_ratio)
    else:
        dest_width = width
        dest_height = int(dest_width * img_ratio)
        
    image = cv2.resize(image, (dest_width, dest_height))
    return image

def join_images(images, gap, direction = "vertical"):
    assert direction in ["vertical", "horizontal"]
    total_images = len(images)
    if direction == "vertical":
        total_height = sum([image.shape[0] for image in images])
        joint_height = total_height + gap * (total_images - 1)
        joint_width = max([image.shape[1] for image in images])
        joint_image = np.ones((joint_height, joint_width, 3)) * 255
        curr_height = - gap
        for image in images:
            curr_height += gap
            joint_image[curr_height: curr_height + image.shape[0], :image.shape[1], :] = image
            curr_height += image.shape[0]
        return joint_image
    elif direction == "horizontal":
        total_width = sum([image.shape[1] for image in images])
        joint_width = total_width + gap * (total_images - 1)
        joint_height = max([image.shape[0] for image in images])
        joint_image = np.ones((joint_height, joint_width, 3)) * 255
        curr_width = - gap
        for image in images:
            curr_width += gap
            joint_image[:image.shape[0], curr_width : curr_width + image.shape[1], :] = image
            curr_width += image.shape[1]
        return joint_image
    else:
        raise ValueError

def initial_bin_pred_df():
    columns = ["camera_view", "gondola_id", "shelf_id", "correct", "not_predicted", "empty_bin", "wrong_seq", "wrong_gondola", "avg_score", "min_score"]
    df = pd.DataFrame(columns=columns)
    return df

def append_bin_pred_df(df, camera_view, gondola_id, shelf_id, correct, not_predicted, empty_bin, wrong_seq, wrong_gondola, avg_score, min_score):
    columns = ["camera_view", "gondola_id", "shelf_id", "correct", "not_predicted", "empty_bin", "wrong_seq", "wrong_gondola", "avg_score", "min_score"]
    row = [[camera_view, gondola_id, shelf_id, correct, not_predicted, empty_bin, wrong_seq, wrong_gondola, avg_score, min_score]]
    curr_row = pd.DataFrame(row, columns = columns)
    df = pd.concat([df, curr_row], ignore_index= True)
    # df = df.append(curr_row, ignore_index = True)
    return df
