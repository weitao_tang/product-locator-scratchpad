from dataclasses import dataclass
from re import L
from cv2 import AgastFeatureDetector_AGAST_7_12d
from matplotlib.colors import PowerNorm
from matplotlib.transforms import Bbox
from proto_tools.codec import StoreCalibration
from proto_tools.codec.components import GondolaId,ShelfId,BinId
from geometry import Camera
from geometry.utils_2d.polygon import Polygon2d
import numpy as np
from aifi_visualization.constants import BLUE, GREEN, GREY, ORANGE, RED
from aifi_visualization.utils import enlarge_fov
from typing import Collection, Dict, Iterable, List, Sequence, Tuple, Union
from aifi_visualization.visualize_components import (
    LineStyle,
    TextStyle,
    visualize_box,
    visualize_component
)
import math
import os
import json
from tqdm import tqdm

import cv2
from pygments import highlight
from sklearn import cluster
import urllib

class ProductQuery():
    def __init__(self, image, bbox, contour, productquery_id):
        self.image = image
        self.bbox = bbox
        self.contour = contour
        self.product_id = productquery_id
        self.area = bbox[2] * bbox[3]
        self.shelf_cluster_id = None
        self.merged_shelf_cluster_id = None
        self.pred_binID = None
        self.pred_gondolaID = None
        self.pred_shelfID = None
        self.dist_to_bottom = None
        self.dist_to_left = None
        self.naive_pred = None

class CameraViewQuery():
    def __init__(self, camera_id, image_path, yolo_txt_path, contain_thresh = 0.6, fill_thresh = 0.6):
        self.camera_id = camera_id
        self.image = cv2.imread(image_path)
        self.box_txt_path = os.path.join(yolo_txt_path)
        self.contain_thresh = contain_thresh
        self.fill_thresh = fill_thresh
        productqueries = self.obtain_productquerys()
        self.productqueries, self.removed_productqueries = self.filter_product_queries(productqueries)

    ####### initialize functions #########
    def obtain_productquerys(self):
        f = open(self.box_txt_path, 'r')
        lines = f.readlines()
        height, width, _ = self.image.shape
        product_queries = []
        for product_id, line in enumerate(lines):
            _, cx_,cy_,w_,h_ = [float(value) for value in line.split(' ')]
            cx,cy,w,h = int(cx_*width), int(cy_*height), int(w_*width), int(h_*height)
            y = cy - int(h/2)
            y = y if y >0 else 0
            x = cx - int(w/2)
            x = x if x>0 else 0
            bbox = [x,y,w,h]
            contour = self.bbox2contour(bbox)
            product_image = self.image[contour[0][1]:contour[2][1], contour[0][0]:contour[2][0], :]
            product_queries.append(ProductQuery(product_image, bbox, contour, product_id))
        return product_queries

    def filter_product_queries(self, productqueries):
        def check_contain(bbox0, bbox1, flag):
            'check if bbox0 contain bbox1'
            x0, y0, w0, h0 = bbox0
            x1, y1, w1, h1 = bbox1
            area1 = w1 * h1
            x_dist = min([x0 + w0, x1 + w1]) - max([x0, x1])
            x_dist = max([x_dist,0]) 
            y_dist = min([y0 + h0, y1 + h1]) - max([y0, y1])
            y_dist = max([y_dist, 0])
            inter_area = x_dist * y_dist
            inter2area1ratio = inter_area/area1
            if flag:
                print(inter2area1ratio)
            return inter2area1ratio > self.contain_thresh

        def check_fill(bbox0, bboxes, flag):
            'check if bboxes fills bbox0'
            x, y, w, h = bbox0
            height, width = self.image.shape[:2]
            origin = np.zeros((width, height))
            origin[x : x + w, y : y + h] = 1
            
            union = np.zeros((width, height))
            for bbox in bboxes:
                x, y, w, h = bbox
                union[x : x + w, y : y + h] = 1
            inter = origin * union
            inter_area = inter.sum()
            area0 = origin.sum()
            if flag:
                print(origin.shape, (x,y,w,h), origin.sum())
                print(inter_area, area0)
            inter2area0 = inter_area/area0
            return inter2area0 > self.fill_thresh

        removed_ids, reserved_ids = [], []
        
        for i, productquery0 in enumerate(productqueries):
            bbox0 = productquery0.bbox
            product_id0 = productquery0.product_id
            contained_bboxes = []
            for j, productquery1 in enumerate(productqueries):
                if i != j:
                    bbox1 = productquery1.bbox
                    product_id1 = productquery1.product_id
                    flag = True if product_id0 == 46 and product_id1 == 461 else False
                    if check_contain(bbox0, bbox1, flag):
                        contained_bboxes.append(bbox1)
            flag = True if product_id0 == 46 else False
            if check_fill(bbox0, contained_bboxes, flag):
                removed_ids.append(product_id0)
            else:
                reserved_ids.append(product_id0)
        
        filtered_productqueries = []
        removed_productqueries = []
        i = 0
        j = len(reserved_ids)
        for productquery in productqueries:
            if productquery.product_id in reserved_ids:
                productquery.product_id = i # resquence product query to make productqueries index same as product id
                i += 1
                filtered_productqueries.append(productquery)
            elif productquery.product_id in removed_ids:
                productquery.product_id = j
                j += 1
                removed_productqueries.append(productquery)
        return filtered_productqueries, removed_productqueries

    def bbox2contour(self, bbox, n=0):
        '''convert bbox(x,y,w,h) to contour(top_left, top_right, bottom_right, bottom_left)
            n is number of points per edge, set as 0 for now
        '''
        assert n == 0
        height, width, _ = self.image.shape
        x,y,w,h = bbox
        yh = y+h if y+h<height else height
        xw = x+w if x+w<width else width
        countor = np.array([[x,y],
                            [xw, y],
                            [xw, yh],
                            [x, yh]])
        return countor

    ####### visualize functions #########
    def plot_product_boxes(self, dest_dir):
        os.makedirs(dest_dir, exist_ok=True)
        image = self.image.copy()
        height, width, _ = image.shape
        for product_query in self.productqueries:
            x,y,w,h = product_query.bbox
            xw = x+w if x+w<width else width
            yh = y+h if y+h<height else height
            image = cv2.rectangle(image, (x,y), (xw,yh), (0,255,0), thickness=1)
        dest_path = os.path.join(dest_dir, f"camera_{self.camera_id}_products.jpg")
        cv2.imwrite(dest_path, image)

    ####### check functions ########

class Planogram():
    def __init__ (self, planogram_path):
        with open(planogram_path) as f:
            planogram_json = json.load(f)
        self.planogram_dict = self.convert_json(planogram_json)
        self.planogram_path = planogram_path
        self.barcode2image_path = self.download_images()
    
    def convert_json(self, json):
        converted_dict = {}
        for strloc, info in json.items():
            loc = eval(strloc)
            converted_dict[loc] = info
        return converted_dict

    def obtain_barcodes_by_loc(self, gondola_id, shelf_id = None, bin_id = None, return_loc = False):
        locs_and_barcodes = []
        if bin_id is not None: # assign gondola, shelf, and bin
            assert shelf_id is not None
            barcode = self.planogram_dict[(gondola_id, shelf_id, bin_id)]['productId']['id']
            loc = (gondola_id, shelf_id, bin_id)
            locs_and_barcodes = [[loc, barcode]]
        elif shelf_id is not None: # assign gondola, shelf
            for loc, info in self.planogram_dict.items():
                if loc[0] == gondola_id and loc[1] == shelf_id:
                    barcode = info['productId']['id']
                    loc = (loc[0], loc[1], loc[2])
                    locs_and_barcodes.append([loc, barcode])
        else: # assign only gondola
            for loc, info in self.planogram_dict.items():
                if loc[0] == gondola_id:
                    barcode = info['productId']['id']
                    loc = (loc[0], loc[1], loc[2])
                    locs_and_barcodes.append([loc, barcode])

        def sort_function(loc_barcode):
            loc = loc_barcode[0]
            return loc[0]*10000*10000 + loc[1]*10000 + loc[2] # faster implementation of comparing locations
        locs_and_barcodes.sort(key=sort_function)
        barcodes = [loc_and_barcode[1] for loc_and_barcode in locs_and_barcodes]
        if not return_loc:
            return barcodes
        else:
            locs = [loc_and_barcode[0] for loc_and_barcode in locs_and_barcodes]
            return barcodes, locs
            
    def obtain_shelf_ids_from_gondola(self, gondola_id):
        shelf_ids = []
        for loc in self.planogram_dict.keys():
            if loc[0] == gondola_id:
                shelf_ids.append(loc[1])
        return list(set(shelf_ids)) # remove duplicates due to multiple bin in a shelf

    def download_images(self):
        planogram_dir = os.path.dirname(self.planogram_path)
        planogram_image_dir = os.path.join(planogram_dir, "planogram_images")
        
        barcode2image_path = {}
        os.makedirs(planogram_image_dir, exist_ok=True)
        for _, (_, info) in tqdm(enumerate(self.planogram_dict.items())):
            barcode = info['productId']['id']
            if 'metadata' in info.keys():
                image_url = info['metadata']['thumbnail']
                barcode = info['productId']['id']
                image_type = os.path.splitext(image_url)[1]
                image_name = barcode + image_type
                image_path = os.path.join(planogram_image_dir, image_name)
                if not os.path.isfile(image_path):
                    urllib.request.urlretrieve(image_url, image_path)
                barcode2image_path[barcode] = image_path
            else:
                barcode2image_path[barcode] = None
        return barcode2image_path
