from cProfile import label
from dataclasses import dataclass
from turtle import width
import matplotlib.pyplot as plt
from proto_tools.codec import StoreCalibration
from proto_tools.codec.components import GondolaId,ShelfId,BinId
from geometry import Camera
from geometry.utils_2d.polygon import Polygon2d
from geometry.box import BoxFace
import numpy as np
import copy
from aifi_visualization.constants import BLUE, GREEN, GREY, ORANGE, RED
from aifi_visualization.utils import enlarge_fov
from typing import Collection, Dict, Iterable, List, Sequence, Tuple, Union
from aifi_visualization.visualize_components import (
    LineStyle,
    TextStyle,
    visualize_box,
    visualize_component
)
from camera_view_process.visionclassifier import VisionClassifier
import math
import os

import cv2
from pygments import highlight
from sklearn import cluster
from camera_view_process.data import ProductQuery, CameraViewQuery, Planogram
from camera_view_process.visionclassifier import VisionClassifier
from camera_view_process.utils import obtain_colors_from_color_map, obtain_vector_3d, obtain_points_on_lineseg_3d, obtain_distance2line3d, rescale_image_fix_ratio, obtain_projection2line2d, join_images


class DTWPredictor():
    """ Using a modified DTW algorithm to assign each product on the shelf to a sku following location constrain.
    """
    def __init__(self, max_skip:int,
                        max_reverse:int,
                        skip_penality = 0.0,
                        reverse_penality = 0.0,
                        end_remove_threshold = 0.5
                        ):
        self.max_skip = max_skip
        self.max_reverse = max_reverse
        self.reverse_penality = reverse_penality
        self.skip_penality = skip_penality
        self.end_remove_threshold = end_remove_threshold

    def predict(self, sim_matrix):
        """ assign each product to a sku by similarity matrix
        """

        # this creats an addtional pseudo bin and pseudo product, to all step pairs not end with last bin and last product
        dist_matrix = 1 - sim_matrix
        N, M  = dist_matrix.shape
        expended_dist_matrix = np.full((N+1, M+1), np.inf)
        expended_dist_matrix[:N, :M] = dist_matrix
        expended_dist_matrix[-1,-1] = 0

        # initial cost matrix for tdw
        cost_matrix = np.zeros((N + 2 + self.max_skip + self.max_reverse, M+2))
        for i in range(0, N + 2 + self.max_skip + self.max_reverse):
            if i != self.max_skip:
                cost_matrix[i, 0] = np.inf
        for i in range(0, self.max_skip + 1):
            for j in range(1, M + 2):
                cost_matrix[i, j] = np.inf
        for i in range(N + 2 + self.max_skip, N + 2 + self.max_skip + self.max_reverse):
            for j in range(0, M + 2):
                cost_matrix[i, j] = np.inf
        
        # initial trace matrix for tdw
        trace_matrix = np.zeros((N+1,M+1), dtype=int)

        # fill in the cost matrix and trace matrix by traditional methology (does not work for reverse)
        # for i in range(N+1):
            # for j in range(M+1):
        for j in range(M+1):
            for i in range(N+1):
                step_cost = []
                for skip in list(range(1, self.max_skip + 1))[::-1]: # skip with penality
                    step_cost.append((self.skip_penality+1)*expended_dist_matrix[i, j] + 
                                        cost_matrix[i + self.max_skip - skip, j]) # curr_sku  = last_sku + skip + 1 
                step_cost.extend([expended_dist_matrix[i, j] + cost_matrix[i+self.max_skip, j], # curr_sku = last_sku + 1 
                                expended_dist_matrix[i, j] + cost_matrix[i + self.max_skip + 1, j]
                                ]) # curr_sku = last_sku
                for reverse in range(1, self.max_reverse + 1):
                    step_cost.append((self.reverse_penality+1)*expended_dist_matrix[i, j] + 
                                        cost_matrix[i + self.max_skip + 1 + reverse, j]
                                        ) # curr_sku = last_sku - reverse
                step_idx = np.argmin(step_cost) # from 0 to max_skip + 1
                cost_matrix[i + self.max_skip + 1, j + 1] = step_cost[step_idx] # curr_sku
                trace_matrix[i,j] = step_idx


        # track back
        i = N 
        j = M 
        path = [(i,j)] # start with the pseduo pair
        total_step = 0
        max_id = self.max_skip + 1
        while i>0 or j>0: # to first product
            trace = trace_matrix[i,j]
            i = i - (max_id - trace) 
            j = j - 1
            total_step += 1
            path.append((i,j))
        
        # might bounce negative products due to cost_matrix size increase caused allowing skip
        path = path[::-1]

        dropped_path = []
        for i, path_ in enumerate(path):
            if path_[0] >=0 and path_[1] >=0 and path_[0] <= N-1 and path_[1] <= M-1:
                dropped_path.append(path_)

        # recalculate cost using poped path
        # assert dropped_path[0][1] == 0 and dropped_path[0][0] >= 0
        total_step = 0
        total_cost = 0
        last_i = -1

        for path_ in dropped_path:
            i, j = path_
            if_skip = (i - last_i) >= 2
            if_reverse = (i - last_i) <= -1
            if if_skip:
                current_cost = dist_matrix[i,j] * (self.skip_penality + 1)
            elif if_reverse:
                current_cost = dist_matrix[i, j] * (self.reverse_penality + 1)
            else:
                current_cost = dist_matrix[i,j]
            total_cost += current_cost
            last_i = i
            total_step += 1 
        avg_cost = float(total_cost)/total_step
        return dropped_path, avg_cost, cost_matrix

    def remove_outside_gondola_products(self, sim_matrix, dtw_path):
        print(dtw_path)

        def obtain_steps_to_remove_from_one_end(dtw_path):
            steps_to_remove = []
            for dtw_step in dtw_path:
                print("step", dtw_step)
                bin, product = dtw_step
                sim = sim_matrix[bin, product]
                if sim > self.end_remove_threshold:
                    print("break")
                    break
                else:
                    steps_to_remove.append(dtw_step)
                    print("steps to remove", steps_to_remove)
            return steps_to_remove

        steps_to_remove_from_left = obtain_steps_to_remove_from_one_end(dtw_path)
        print(steps_to_remove_from_left)
        steps_to_remove_from_right = obtain_steps_to_remove_from_one_end(dtw_path[::-1])
        print(steps_to_remove_from_right)

        removed_steps = steps_to_remove_from_left + steps_to_remove_from_right

        path = []
        for dtw_step in dtw_path:
            if not dtw_step in removed_steps:
                path.append(dtw_step)

        return path
            


        #     bin, product = dtw_step
        #     if bin == 0:
        #         start_bin_products.append(product)
        #     elif bin == (total_bins - 1):
        #         end_bin_products.append(product)

        # removed_products = []
        # # remove from start, until first product has sim larger than threshold
        # if len(start_bin_products) > 1:
        #     for product in start_bin_products:
        #         sim = sim_matrix[0, product]
        #         if sim < end_remove_threshold:
        #             removed_products.append(product)
        #         else:
        #             break

        # trigger = False
        # # remove to end, remove products which are smaller than threshold
        # if len(end_bin_products) > 1:
        #     for product in end_bin_products:
        #         sim = sim_matrix[-1, product]
        #         print("sim", sim)
        #         if sim < end_remove_threshold:
        #             trigger = True
        #         print("trigger", trigger)
        #         if trigger:
        #             removed_products.append(product)
        #         print(removed_products)
        # return removed_products