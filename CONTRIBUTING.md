# Contributing
Contributions are welcome! Submit a merge request to `master`. Make sure you follow the guidelines below before contributing.


## Getting Started

### Setup a virtual env
We highly recommend developing in some sort of [virtual environment](https://packaging.python.org/tutorials/installing-packages/#creating-virtual-environments) such as:
- [`venv`](https://docs.python.org/3/library/venv.html) Built into python since 3.3.
- [`conda`](https://docs.conda.io/projects/conda/en/latest/user-guide/tasks/manage-environments.html) Useful when you also have lots of CV dependencies.

### Configure `pip`
AiFi hosts packages in a [private registry](https://gitlab.com/groups/aifi-ml/-/packages). You will need to point `pip` at this registry to install AiFi libraries.
- [Create a token](https://gitlab.com/-/profile/personal_access_tokens) to use below.
- Configure pip to point at AiFi's package registry
```shell
export GITLAB_TOKEN=<your token>
pip config set global.index-url https://gitlab-ci-token:${GITLAB_TOKEN}@gitlab.com/api/v4/groups/1297998/-/packages/pypi/simple
pip config set global.extra-index-url https://pypi.org/simple
```
This will prioritize packages in AiFi's registry over pypi, falling back to pypi only if a package is not found in AiFi's registry.

### Install dev dependencies
We have included dev and test dependencies for your convenience.
#### bash
```bash
pip install -e .[test,dev]
```
#### zsh
```zsh
pip install -e .'[test,dev]'
```

### Install pre-commit hooks
The pre-commit hooks automate the linting and code formatting guidelines.
```bash
pre-commit install
```


## Guidelines
Please follow these guidelines when contributing.

### Commit messages
Please ensure your commit messages follow the [angular commit message conventions](https://github.com/angular/angular.js/blob/master/DEVELOPERS.md#-git-commit-guidelines) so that [`semantic-release`](https://github.com/semantic-release/semantic-release) can automatically update the version.

### Code formatting
Format your code before submitting (included in [pre-commit hooks](#Install Pre-commit Hooks)).
```bash
black -l 79 .
```

### Linting
Ensure all linting errors are resolved before submitting (included in [pre-commit hooks](#Install Pre-commit Hooks)).
```bash
flake8 .
```

### Testing
Ensure your code passes the unittests.
```bash
python -m unittest tests/
```

### Coverage
Please add unittest coverage for any new code. You can check coverage with:
```bash
coverage run --source=example_project/ -m unittest discover -s tests/
coverage report -m
```

### Docstrings
Ensure you have [google-style docstrings](https://sphinxcontrib-napoleon.readthedocs.io/en/latest/example_google.html) on every public (and preferably private) classes, modules, and functions.

### Generating Usage Docs
Autogenerate the usage documentation with typer-cli
```bash
typer example_project/main.py utils docs --name example-project --output USAGE.md
```

### Style
Code is written once, but read many times. Take a little time to clean up your code and make life easier for those reading it. Follow standard coding best practices such as (but not limited to):
- Use descriptive variable names and avoid abbreviation.
- Write [DRY Code](https://en.wikipedia.org/wiki/Don%27t_repeat_yourself)
- Establish [consistent abstraction levels](https://softwareengineering.stackexchange.com/questions/110933/how-to-determine-the-levels-of-abstraction).
- Ensure classes and functions [do only one thing](https://stackoverflow.com/questions/49871724/a-method-should-do-one-thing-once-and-only-once-what-does-that-mean)
- Reduce nesting with [Line-of-site Code](https://medium.com/@matryer/line-of-sight-in-code-186dd7cdea88)
- Ensure top-down readability so that the reader doesn't need to skip around.

When in doubt, refer to the [Zen of Python](https://www.python.org/dev/peps/pep-0020/).
```
Beautiful is better than ugly.
Explicit is better than implicit.
Simple is better than complex.
Complex is better than complicated.
Flat is better than nested.
Sparse is better than dense.
Readability counts.
Special cases aren't special enough to break the rules.
Although practicality beats purity.
Errors should never pass silently.
Unless explicitly silenced.
In the face of ambiguity, refuse the temptation to guess.
There should be one-- and preferably only one --obvious way to do it.
Although that way may not be obvious at first unless you're Dutch.
Now is better than never.
Although never is often better than *right* now.
If the implementation is hard to explain, it's a bad idea.
If the implementation is easy to explain, it may be a good idea.
Namespaces are one honking great idea -- let's do more of those!
```

## Building Locally
You can also build the containerized version of this service. You must specify:
- `GITLAB_TOKEN`: A gitlab token to use so that we can download private dependencies from `aifi-ml`.
- `PROJECT_REGISTRY`: A container registry for this project.
```
docker build --build-arg TOKEN=$GITLAB_TOKEN -f deployments/docker/Dockerfile . -t $PROJECT_REGISTRY:local
```


## Maintainers
- Maintainer's name (email@aifi.io)
